# ping-scan
Ping scan is a script to find IP addresses online in local network. To work this script ICMP protocol needs to be enable or unblock in firewall of remote machines.


<pre>
Use this script ./ping-scan.sh  [IP Address] Last octet of [Starting IP] [End IP]
{example}:
          ./ping-scan.sh  192.168.16.1  20  80

In the above example first argument is the Network IP address (192.168.16.1),
second argument is last octet of starting IP address (20) and the third argument is
last octect of ending IP address (80) of the range to scan.

For simple manual execute script with out any argument.
</pre>
