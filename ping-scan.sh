#!/bin/bash
# Author: Sanju P Debnath
# git-repo: https://gitlab.com/linuxcli/pingscan

MSG='This script only works if ICMP port is enable on remote machine'
HELP='Use this script ./ping-scan.sh [IP Address] Last octet of [Starting IP] [End IP]
{example}:
           ./ping-scan.sh 192.168.16.1 20 80

In the above example, the first argument is the Network IP address (192.168.16.1),
the second argument is the last octet of starting IP address (20) and the third argument is
last octet of ending IP address (80) of the range to scan.

For simple manual execute the script without any argument.'

if [ "$1" = "" ]
then
echo "$HELP"
exit 0
fi

IP="$1"
OCTET1=$(echo "$IP" |awk -F. '{print $1}')
OCTET2=$(echo "$IP" |awk -F. '{print $2}')
OCTET3=$(echo "$IP" |awk -F. '{print $3}')

if [ "$OCTET1" -le "254" ]  2> /dev/null
then
OCTET1="$OCTET1"
else
printf "\nInvalid IP address in first octet $OCTET1\n"
fi

if [ "$OCTET2" -le "254" ]  2> /dev/null
then
OCTET2="$OCTET2"
else
printf "\nInvalid IP address in second octet $OCTET2\n"
fi

if [ "$OCTET3" -le "254" ] 2> /dev/null
then
OCTET3="$OCTET3"
else
printf "\nInvalid IP address in third octet $OCTET3\n"
fi

IPS="$OCTET1.$OCTET2.$OCTET3"

if [ "$2" -le "254" ] 2> /dev/null
then
FNUM="$2"
else
printf "\nStarting IP address from a range is Invalid\n"
fi

if [ "$3" -le "254" ]   2> /dev/null
then
LNUM="$3"
else
printf "\nEnding IP address from a range is Invalid\n"
fi

RANG=$(seq "$FNUM" "$LNUM" 2> /dev/null)

if [ "$RANG" ]
then
echo "$MSG"
for P in  $RANG
do 
EXEC_PING=$(ping -nw1  "$IPS.$P" | grep 'icmp_')
if   [ -n "$EXEC_PING" ]
then  
printf "$IPS.$P is \e[1;32monline\e[0m\n"
elif [ -z "$EXEC_PING" ]
then
printf "$IPS.$P is \e[1;31moffline\e[0m\n"
fi
done

else
printf "\nInvalid IP range $FNUM $LNUM\n"
fi
